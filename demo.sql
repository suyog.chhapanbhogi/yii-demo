-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2018 at 03:01 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0-Inactive, 1-Active',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `mobile`, `fullname`, `address`, `status`, `created`) VALUES
(1, 'abhi@gmail.com', 'test', '', '', '', 0, '2018-01-02 10:43:58'),
(2, 'kumar@gmail.com', 'test', '', '', '', 0, '2018-01-02 10:44:30'),
(3, 'vikas@gmail.com', 'test', '8093763084', 'Vikas', 'Patia', 1, '2018-01-02 12:37:38'),
(4, 'vikram@gmail.com', 'test', '8093763084', 'Vikram', 'Patia', 1, '2018-01-02 12:39:35'),
(5, 'vikram@gmail.com', 'test', '8093763084', 'Vikram', 'Patia', 1, '2018-01-02 12:41:56'),
(6, 'anand@gmail.com', 'test', '8093763084', 'Anand', 'Patia', 1, '2018-01-02 12:42:50'),
(7, 'abc@gmail.com', 'test', '8093763084', 'abc', 'Patia', 1, '2018-01-02 12:43:48'),
(8, 'akash@gmail.com', 'test', '8888888888', 'akash', 'bbsr', 1, '2018-01-02 12:54:58'),
(9, 'abhinandan@gmail.com', 'test', '8093763084', 'Abhinandan', 'bbsr', 1, '2018-01-02 13:00:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
