<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model {

    public $fullname;
    public $email;
    public $mobile;
    public $password;
    public $address;
    public $rememberMe = true;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['fullname', 'fullname'], 'required'],
            [['email', 'email'], 'required'],
            [['address', 'address'], 'required'],
            [['mobile', 'mobile'], 'required'],
        ];
    }

    public function register() {
        $data = $_POST['RegisterForm'];
        $sql = "insert into user (email,password,mobile,fullname,address,status) values ('" . $data['email'] . "','" . $data['password'] . "','" . $data['mobile'] . "','" . $data['fullname'] . "','" . $data['address'] . "',1)";
        $parameters = $data;

        Yii::$app->db->createCommand($sql)->execute();
        return 1;
    }

}
