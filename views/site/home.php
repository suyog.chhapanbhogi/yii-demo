<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'User Home Page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Welcome <b><?= $user['email'] ?></b> You have been logged in successfully.
    </p>

    <!--<code><?= __FILE__ ?></code>-->
</div>
