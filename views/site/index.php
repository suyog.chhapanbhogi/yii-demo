<?php
/* @var $this yii\web\View */

$this->title = 'YII Demo Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome</h1>

        <p class="lead">Demo Application For Signin and Signup</p>
    </div>

    <div class="body-content">
        <?php if (isset($message)) { ?>
            <center><h4><font color="green"><?= $message ?></font></h4></center>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-2">&nbsp; </div>
                    <div class="col-lg-8">
                        <h2>Application</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                            fugiat nulla pariatur.</p>

                    </div>
                    <div class="col-lg-2">&nbsp;</div>
                </div>

                </div>
                </div>
